### jadmin：返璞归真的邂逅

jadmin是基于jsyso + LayUI搭建的后台管理系统，目前已自带权限控制模块，多选项卡操作。

### 部分截图

![jadmin](images/1.jpg)

![jadmin](images/2.jpg)

![jadmin](images/3.jpg)

![jadmin](images/4.jpg)

后台测试地址：[http://jadmin.jsyso.com/](http://jadmin.jsyso.com/)

测试账号密码：jsyso/jsyso（还请高抬贵手，勿改动密码）

### 更多问题

更多问题欢迎QQ加好友：514737546（加时还请说明(～￣▽￣)～）

