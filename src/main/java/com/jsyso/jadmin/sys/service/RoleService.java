package com.jsyso.jadmin.sys.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.jsyso.jadmin.common.service.BaseService;
import com.jsyso.jsyso.db.Dao;
import com.jsyso.jsyso.util.JsMap;

@Service
public class RoleService extends BaseService {
	// 系统超级管理员
	public static final String SYS_SUPER_ADMIN = "sys_super_admin";
	
	private Dao roleMenuDao = Dao.get("sys_role_menu", "role_id", "menu_id");
	
	@Override
	protected Dao dao() {
		return Dao.get("sys_role");
	}
	
	public List<JsMap> roleMenuByRoleId(Long roleId) {
		return roleMenuDao.select(JsMap.create("role_id", roleId));
	}
	
	public int addRoleMenuAll(List<JsMap> datas) {
		return roleMenuDao.insertAll(datas);
	}
	
	public int deleteRoleMenu(JsMap data) {
		return roleMenuDao.delete(data);
	}
}
