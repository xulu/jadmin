package com.jsyso.jadmin.sys.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.jsyso.jadmin.common.config.Global;
import com.jsyso.jadmin.common.utils.ShiroUtils;
import com.jsyso.jadmin.common.utils.TreeUtils;
import com.jsyso.jadmin.common.web.BaseController;
import com.jsyso.jadmin.sys.service.MenuService;
import com.jsyso.jadmin.sys.utils.UserUtils;
import com.jsyso.jsyso.json.jackson.JsonMapper;
import com.jsyso.jsyso.util.JsMap;
import com.jsyso.jsyso.web.JsMapParam;

/**
 * 菜单控制器
 * @author janjan, xujian_jason@163.com
 *
 */
@Controller
@RequestMapping(value = "/a/sys/menu")
public class MenuController extends BaseController {
	
	@Autowired
	private MenuService menuService;
	
	@ModelAttribute("menu")
	public Object get(@RequestParam(
			required=false, defaultValue="0") Long id
			,@RequestParam(required=false, defaultValue="0") Long parentId) {
		JsMap result = null;
		if(id != null && id > 0) 
			result = menuService.get(id);
		if(result == null)
			result = JsMap.create(12)
			.set("id", 0)
			.set("name", "")
			.set("href", "")
			.set("icon", "")
			.set("isShow", 1)
			.set("permission", "")
			.set("parentId", parentId);
		return result.getBean();
	}
	
	@RequiresPermissions("sys:menu:view")
	@RequestMapping(value = {"list", ""})
	public String list(
			HttpServletRequest request, HttpServletResponse response, Model model) {
		List<JsMap> list = menuService.findList(JsMap.create("del_flag", Global.NO));
		Map<String, List<JsMap>> treeMap = TreeUtils.create(list).getParentMap();
		model.addAttribute("treeJson", JsonMapper.getInstance().toJson(treeMap));
		return this.display("sys/menu_list");
	}
	
	@RequiresPermissions("sys:menu:view")
	@RequestMapping(value = "form")
	public String form(
			Long parentid,
			Long id,
			HttpServletRequest request, HttpServletResponse response, Model model) {
		return this.display("sys/menu_form");
	}
	
	@RequiresPermissions("sys:menu:edit")
	@RequestMapping(value = "do_save", method = RequestMethod.POST)
	public void doSave(
			JsMapParam menu, 
			HttpServletRequest request, HttpServletResponse response, Model model) {
		JsMap data = menu.getData();
		if(data.get("id", 0l, Long.class) > 0)
			menuService.save(menuService.keyRename(data));
		else
			menuService.add(menuService.keyRename(data));
		// 如果是超级管理员，清除超级管理员菜单缓存
		if(UserUtils.isSysAdmin())
			ShiroUtils.removeCache(UserUtils.CACHE_MENU_LIST);
		success(response, "");
	}
	
	@RequiresPermissions("sys:menu:edit")
	@RequestMapping(value = "do_delete")
	public void doDelete(
			Long id,
			HttpServletRequest request, HttpServletResponse response, Model model) {
		if(id == null || id < 1)
			error(response, "参数错误");
		menuService.delete(JsMap.create("id", id));
		success(response, "");
	}
	
}
