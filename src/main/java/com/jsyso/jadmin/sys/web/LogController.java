package com.jsyso.jadmin.sys.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.jsyso.jadmin.common.config.Global;
import com.jsyso.jadmin.common.web.BaseController;
import com.jsyso.jadmin.sys.service.LogService;
import com.jsyso.jadmin.sys.service.UserService;
import com.jsyso.jsyso.util.AbsHandler;
import com.jsyso.jsyso.util.Arrays;
import com.jsyso.jsyso.util.JsMap;

/**
 * 日志控制器
 * @author janjan, xujian_jason@163.com
 *
 */
@Controller
@RequestMapping(value = "/a/sys/log")
public class LogController extends BaseController {
	
	@Autowired
	private LogService logService;
	@Autowired
	private UserService userService;
	
	@SuppressWarnings("unchecked")
	@RequiresPermissions("sys:log:view")
	@RequestMapping(value = {"list", ""})
	public String list(
			@RequestParam(required=false, defaultValue="1")
			int pageNo,
			HttpServletRequest request, HttpServletResponse response, Model model) {
		JsMap page = logService.findListPage(JsMap.create("del_flag", Global.NO), "id DESC", pageNo, 20);
		Arrays.each((List<JsMap>) page.get("list"), new AbsHandler<JsMap>() {
			public void doHandle(JsMap param) {
				param.put("createByUser", userService.get(param.get("createBy", 0l, Long.class)));
			}
		});
		model.addAllAttributes(page);
		return this.display("sys/log_list");
	}

	@RequiresPermissions("sys:log:view")
	@RequestMapping(value = "form")
	public String form(
			Long id,
			HttpServletRequest request, HttpServletResponse response, Model model) {
		return this.display("sys/log_form");
	}
	
}
