package com.jsyso.jadmin.common.web;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsyso.jsyso.web.WebUtils;

public abstract class BaseController {
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * 重定向跳转
	 */
	protected String redirect(String path) {
		return "redirect:" + path;
	}
	
	/**
	 * 显示view
	 */
	protected String display(String path) {
		return path;
	}
	
	/**
	 * 客户端返回json字符串
	 */
	protected String renderString(HttpServletResponse response, Object object) {
		return WebUtils.renderString(response, object);
	}
	
	/**
	 * 返回成功json字符串
	 */
	protected String success(HttpServletResponse response, Object data) {
		return renderString(response, ResponseData.success(data));
	}
	
	/**
	 * 返回失败json字符串
	 */
	protected String error(HttpServletResponse response, String msg) {
		return renderString(response, ResponseData.error(msg));
	}
	
}
