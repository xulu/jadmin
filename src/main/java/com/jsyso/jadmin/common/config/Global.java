package com.jsyso.jadmin.common.config;

import java.util.Properties;

import org.apache.commons.lang3.StringUtils;

import com.jsyso.jsyso.spring.Springs;
import com.jsyso.jsyso.util.CastUtils;

/**
 * 全局配置类
 * @author janjan, xujian_jason@163.com
 *
 */
public class Global {
	// 是/否
	public static final int YES = 1;
	public static final int NO 	= 0;
	
	private static Properties properties = Springs.getBean("prop", Properties.class);
	
	/**
	 * 获取配置
	 */
	public static String getConfig(String key) {
		return properties.getProperty(key);
	}
	
	public static String getConfig(String key, String defaultValue) {
		String value = getConfig(key);
		return StringUtils.isBlank(value) ? defaultValue : value;
	}
	
	public static <T> T getConfig(String key, Class<T> clazz) {
		String value = getConfig(key);
		return CastUtils.cast(value, clazz);
	}
	
}
