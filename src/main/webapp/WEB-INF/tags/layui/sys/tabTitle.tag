<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/layui/_public/taglib.jsp"%>
<%@ attribute name="url" type="java.lang.String" required="true" description="请求地址"%>
<%@ attribute name="title" type="java.lang.String" required="true" description="标题"%>
<li ${empty param.id ? 'class="layui-this" ' : '' }data-url="${__ADMIN__ }/${url }/">添加${title }</li>
<c:if test="${not empty param.id }">
<li class="layui-this" data-url="${__ADMIN__ }/${url }?id=${param.id }">编辑${title }</li>
</c:if>