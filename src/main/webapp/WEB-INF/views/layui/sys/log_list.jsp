<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/layui/_public/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>Powered By ${fns:getConfig('text.productName')}</title>
<%@include file="../_public/header.jsp" %>
</head>
<body>
<div class="sub-page">
	<div class="layui-tab layui-tab-brief sub-page-tab" lay-filter="F_sub_tab">
		<ul class="layui-tab-title">
			<li class="layui-this" data-url="${__ADMIN__ }/sys/log/">操作日志</li>
		</ul>
		<div class="layui-tab-content">
			<div class="layui-tab-item layui-show">
				<div class="layui-form">
					<table class="layui-table">
						<colgroup>
							<col width="5%">
							<col width="20%">
							<col width="15%">
							<col width="15%">
							<col>
						</colgroup>
						<thead>
							<tr>
								<th>ID</th>
								<th>标题</th>
								<th>操作者</th>
								<th>操作时间</th>
								<th>操作内容</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${list }" var="item">
							<tr>
								<td>${item.id }</td>
								<td>${item.title }</td>
								<td>${item.createByUser.name }</td>
								<td>${item.createDate }</td>
								<td>
									<p style="color: #009688;">操作IP：</p>
									<p>${item.remoteAddr }</p>
									<p style="color: #009688;">请求地址：</p>
									<p>${item.requestUri }</p>
									<p style="color: #009688;">操作方式：</p>
									<p>${item.method }</p>
									<p style="color: #009688;">提交的数据：</p>
									<p>${item.params }</p>
								</td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div id="pages"></div>
			</div>
		</div>
	</div>
</div>
<script src="${__STATIC__ }/admin/js/admin.js?t=<%=System.currentTimeMillis() %>"></script>
<script type="text/javascript">
layui.use(['laypage','element'], function(){
	var $ = layui.jquery
	,layer = layui.layer
	,laypage = layui.laypage
	,element = layui.element();
	
	laypage({
		cont: 'pages'
	    ,pages: ${pages}
	    ,groups: 5
	    ,curr: ${pageNo}
	    ,jump: function(obj, first){
	    	first || (window.location.href = "${__ADMIN__ }/sys/log?pageNo=" + obj.curr);
		}
	});
});
</script>
</body>
</html>