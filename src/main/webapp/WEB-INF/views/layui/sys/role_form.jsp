<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/layui/_public/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>Powered By ${fns:getConfig('text.productName')}</title>
<%@include file="../_public/header.jsp" %>
</head>
<body>
<div class="sub-page">
	<div class="layui-tab layui-tab-brief sub-page-tab" lay-filter="F_sub_tab">
		<ul class="layui-tab-title">
			<li data-url="${__ADMIN__ }/sys/role/">角色管理</li>
			<!-- 
			<li ${empty param.id ? 'class="layui-this" ' : '' }data-url="${__ADMIN__ }/sys/role/form/">添加角色</li>
			<c:if test="${not empty param.id }">
			<li class="layui-this" data-url="${__ADMIN__ }/sys/role/form?id=${param.id }">编辑角色</li>
			</c:if>
			 -->
			 <sys:tabTitle url="sys/role/form" title="角色"></sys:tabTitle>
		</ul>
		<div class="layui-tab-content">
			<div class="layui-tab-item layui-show">
				<form:form class="layui-form" id="inputForm" modelAttribute="role" action="${__ADMIN__}/sys/role/do_save" method="post">
					<form:hidden path="id"/>
					<div class="layui-form-item">
						<label class="layui-form-label">名称</label>
						<div class="layui-input-inline">
							<input type="text" name="name" value="${role.name }" lay-verify="required" class="layui-input">
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">英文名</label>
						<div class="layui-input-inline">
							<input type="text" name="enname" value="${role.enname }" lay-verify="required" class="layui-input">
						</div>
						<div class="layui-form-mid layui-word-aux">角色标识，系统唯一（例如：project_manager）</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">是否启用</label>
						<div class="layui-input-block">
							<input type="hidden" id="J_hdn_status" name="status" value="${role.status }" />
							<input type="checkbox"${role.status eq 1 ? ' checked=""' : '' } lay-verify="required" lay-skin="switch" 
							lay-filter="F_switch" data-hdnid="#J_hdn_status" lay-text="ON|OFF">
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">角色描述</label>
						<div class="layui-input-inline">
							<textarea placeholder="请输入角色描述" name="remark" class="layui-textarea">${role.remark }</textarea>
						</div>
					</div>
					<div class="layui-form-item">
						<div class="layui-input-block">
							<button class="layui-btn" data-listurl="${__ADMIN__ }/sys/role/" lay-submit="" lay-filter="F_do_ajax_submit">确认提交</button>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>
<script src="${__STATIC__ }/admin/js/admin.js?t=<%=System.currentTimeMillis() %>"></script>
<script type="text/javascript">
layui.use(['element'], function(){
	var $ = layui.jquery
	,layer = layui.layer
	,element = layui.element();
	
});
</script>
</body>
</html>