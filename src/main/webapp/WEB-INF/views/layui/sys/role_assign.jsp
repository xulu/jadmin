<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/layui/_public/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>Powered By ${fns:getConfig('text.productName')}</title>
<%@include file="../_public/header.jsp" %>
<style type="text/css">
.layui-form-checkbox[lay-skin=primary] span {
	color: #000;
}
.layui-form-item .layui-form-checkbox[lay-skin=primary] {
	margin-top: 0;
}
</style>
</head>
<body>
<div class="sub-page">
	<div class="layui-tab layui-tab-brief sub-page-tab" lay-filter="F_sub_tab">
		<ul class="layui-tab-title">
			<li data-url="${__ADMIN__ }/sys/role/">角色管理</li>
			<li data-url="${__ADMIN__ }/sys/role/form/">添加角色</li>
			<li class="layui-this" data-url="${__ADMIN__ }/sys/role/assign?id=${param.id }">权限分配</li>
		</ul>
		<div class="layui-tab-content">
			<div class="layui-tab-item layui-show">
				<form:form id="inputForm" class="layui-form" modelAttribute="role" action="${__ADMIN__ }/sys/role/do_assign" method="post">
					<form:hidden path="id"/>
					<div class="layui-form-item">
					<table class="layui-table tree-table" lay-skin="line">
						<colgroup>
							<col>
						</colgroup>
						<tbody>
							<c:forEach items="${fns:getChainTree() }" var="item">
							<tr id="J_node_${item.id }" data-id="${item.id }" class="tree-table__children children-${item.parentId }" style="display:none;">
								<td>
									<!-- 距离空格 -->
									<c:forEach begin="2" end="${item.level }" step="1">
									<span class="tree-table__nbsp"></span>
									</c:forEach>
									<!-- 箭头 -->
									<c:if test="${not empty item.children }">
									<i class="fa tree-table-icon tree-table-spread"></i>
									</c:if>
									<!-- 复选框 -->
									<input type="checkbox" name="ids" value="${item.id }" lay-filter="F_cbx_tree" lay-skin="primary" title="${item.name }">
								</td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
					</div>
					<div class="layui-form-item">
						<button class="layui-btn" data-listurl="${__ADMIN__ }/sys/role/" lay-submit="" lay-filter="F_jquery_ajax_submit">确认保存</button>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>
<script src="${__STATIC__ }/admin/js/admin.js?t=<%=System.currentTimeMillis() %>"></script>
<script type="text/javascript">
layui.use(['element','form'], function(){
	var $ = layui.jquery
	,layer = layui.layer
	,element = layui.element()
	,form = layui.form()
	,menuIds = ${menuIdString };
	
	function _treeCheck($elem, checked) {
		$elem.prop("checked", checked);
		var id = $elem.parents('.tree-table__children').data('id');
		$(".children-" + id).each(function() {
			_treeCheck($(this).find('input[name="ids"]'), checked);
		});
	}
	
	// 监听checkbox
	form.on('checkbox(F_cbx_tree)', function(data){
		_treeCheck($(data.elem), data.elem.checked);
		form.render('checkbox');
	});
	
	// 显示第一级
	$(".children-0").show();
	$.each(menuIds, function(idx, id) {
		$("#J_node_" + id).find('input[name="ids"]').prop("checked", true);
		form.render('checkbox');
	});
	$(".tree-table__children .tree-table-spread").on('click', function() {
		$(this).hasClass('tree-table-spread__open') ? 
				$(this).removeClass('tree-table-spread__open') : $(this).addClass('tree-table-spread__open');
		var id = $(this).parents('.tree-table__children').data('id');
		$(".children-" + id).toggle();
	});
});
</script>
</body>
</html>