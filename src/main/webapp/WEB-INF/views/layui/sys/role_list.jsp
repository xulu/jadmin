<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/layui/_public/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>Powered By ${fns:getConfig('text.productName')}</title>
<%@include file="../_public/header.jsp" %>
</head>
<body>
<div class="sub-page">
	<div class="layui-tab layui-tab-brief sub-page-tab" lay-filter="F_sub_tab">
		<ul class="layui-tab-title">
			<li class="layui-this" data-url="${__ADMIN__ }/sys/role/">角色管理</li>
			<li data-url="${__ADMIN__ }/sys/role/form/">添加角色</li>
		</ul>
		<div class="layui-tab-content">
			<div class="layui-tab-item layui-show">
				<div class="layui-form">
					<table class="layui-table">
						<colgroup>
							<col width="10%">
							<col width="15%">
							<col width="20%">
							<col width="10%">
							<col width="20%">
							<col>
						</colgroup>
						<thead>
							<tr>
								<th>ID</th>
								<th>角色名</th>
								<th>角色英文名</th>
								<th>是否启用</th>
								<th>添加时间</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${list }" var="item">
							<tr>
								<td>${item.id }</td>
								<td>${item.name }</td>
								<td>${item.enname }</td>
								<td>
									<i class="layui-icon menu-isshow ${item.status eq 1 ? 'menu-isshow_y' : 'menu-isshow_n' }">${item.status eq 1 ? '&#xe605;' : '&#x1006;' }</i>
								</td>
								<td>${item.createDate }</td>
								<td>
									<div class="layui-btn-group">
										<button data-url="${__ADMIN__ }/sys/role/assign?id=${item.id }" class="do-action layui-btn layui-btn-small">分配权限</button>
										<button data-url="${__ADMIN__ }/sys/role/form?id=${item.id }" class="do-action layui-btn layui-btn-small">编辑</button>
										<button data-url="${__ADMIN__ }/sys/role/do_delete?id=${item.id }" data-type="ajaxDelete" class="do-action layui-btn layui-btn-small">删除</button>
									</div>
								</td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="${__STATIC__ }/admin/js/admin.js?t=<%=System.currentTimeMillis() %>"></script>
<script type="text/javascript">
layui.use(['element'], function(){
	var $ = layui.jquery
	,layer = layui.layer
	,element = layui.element();
});
</script>
</body>
</html>