<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/layui/_public/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>Powered By ${fns:getConfig('text.productName')}</title>
<%@include file="../_public/header.jsp" %>
</head>
<body>
<div class="sub-page">
	<div class="layui-tab layui-tab-brief sub-page-tab" lay-filter="F_sub_tab">
		<ul class="layui-tab-title">
			<li data-url="${__ADMIN__ }/sys/user/">管理员</li>
			<!-- 
			<li ${empty param.id ? 'class="layui-this" ' : '' }data-url="${__ADMIN__ }/sys/user/form/">添加管理员</li>
			<c:if test="${not empty param.id }">
			<li class="layui-this" data-url="${__ADMIN__ }/sys/user/form?id=${param.id }">编辑管理员</li>
			</c:if>
			 -->
			<sys:tabTitle url="sys/user/form" title="管理员"></sys:tabTitle>
		</ul>
		<div class="layui-tab-content">
			<div class="layui-tab-item layui-show">
				<form:form class="layui-form" id="inputForm" modelAttribute="user" action="${__ADMIN__}/sys/user/do_save" method="post">
					<form:hidden path="id"/>
					<input type="hidden" name="oldPassword" value="${user.password }" />
					<div class="layui-form-item">
						<label class="layui-form-label">登录名</label>
						<div class="layui-input-inline">
							<input type="text" name="loginName" value="${user.loginName }" lay-verify="required" class="layui-input">
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">密码</label>
						<div class="layui-input-inline">
							<input type="password" name="password" value="${user.password }" lay-verify="required" autocomplete="off" class="layui-input">
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">真实姓名</label>
						<div class="layui-input-inline">
							<input type="text" name="name" value="${user.name }" lay-verify="required" class="layui-input">
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">角色</label>
						<div class="layui-input-block">
							<c:forEach items="${roles }" var="item">
							<input type="checkbox" id="J_role_${item.id }" lay-filter="F_cbx" value="${item.id }" 
							name="roles" lay-skin="primary" title="${item.name }">
							</c:forEach>
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">是否启用</label>
						<div class="layui-input-block">
							<input type="hidden" id="J_hdn_loginFlag" name="loginFlag" value="${user.loginFlag }" />
							<input type="checkbox"${user.loginFlag eq 1 ? ' checked=""' : '' } lay-verify="required" lay-skin="switch" 
							lay-filter="F_switch" data-hdnid="#J_hdn_loginFlag" lay-text="ON|OFF">
						</div>
					</div>
					<div class="layui-form-item">
						<div class="layui-input-block">
							<button class="layui-btn" data-listurl="${__ADMIN__ }/sys/user/" lay-submit="" lay-filter="F_jquery_ajax_submit">确认提交</button>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>
<script src="${__STATIC__ }/admin/js/admin.js?t=<%=System.currentTimeMillis() %>"></script>
<script type="text/javascript">
layui.use(['element','form'], function(){
	var $ = layui.jquery
	,layer = layui.layer
	,element = layui.element()
	,form = layui.form()
	,userRoles = ${userRoles };
	
	$.each(userRoles||[], function(i, id){
		$("#J_role_" + id).prop("checked", true);
	});
	form.render('checkbox');
});
</script>
</body>
</html>