<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/layui/_public/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>首页 -  Powered By ${fns:getConfig('text.productName')}</title>
<%@include file="../_public/header.jsp" %>
</head>
<body>
<div class="layui-layout main-index layui-layout-admin">
	<div class="layui-header frame-header-xsm frame-header header">
		<a class="logo-xsm logo hidden-md hidden-lg" href="${__ADMIN__ }">
	 		<img src="${__STATIC__ }/images/logo.png" alt="jsyso-admin">
	    </a>
		<div class="layui-main">
			<a class="logo hidden-xs hidden-sm" href="${__ADMIN__ }">
		      <img src="${__STATIC__ }/images/logo.png" alt="jsyso-admin">
		    </a>
		    <ul class="layui-nav layui-nav-xsm">
		    	<li class="layui-nav-item layui-nav-item-xsm hidden-md hidden-lg">
					<a href="javascript:;" id="J_open_nav"><i class="fa fa-bars"></i></a>
				</li>
				<li class="layui-nav-item layui-nav-item-xsm">
					<a href="javascript:;"><i class="layui-icon">&#xe60c;</i>
					欢迎，${fns:getUser().name}<span class="layui-nav-more"></span></a>
					<dl class="layui-nav-child">
						<dd>
							<a href="javascript:;" class="do-open-nav" data-url="/sys/user/update-pwd" data-id="user_update_pwd"><i class="fa fa-lock"></i><cite>修改密码</cite></a>
						</dd>
						<dd>
							<a href="${__ADMIN__ }/logout"><i class="fa fa-sign-out"></i><cite>退出</cite></a>
						</dd>
					</dl>
				</li>
			</ul>
		</div>
	</div>
	<div class="layui-side layui-side-xsm layui-bg-black">
		<div class="layui-side-scroll">
			<ul class="layui-nav layui-nav-tree index-nav" lay-filter="F_index_nav">
				<c:forEach items="${fns:getTreeMenuList()}" var="item">
				<li class="layui-nav-item">
					<a href="javascript:;">
						<i class="fa ${item.icon }"></i><cite>${item.name }</cite>
						<span class="layui-nav-more"></span>
					</a>
					<dl class="layui-nav-child">
						<c:forEach items="${item.children}" var="chilItem">
						<c:if test="${chilItem.isShow eq 1 && not empty chilItem.href}">
						<dd data-url="${chilItem.href }" data-id="${chilItem.id }">
							<a href="javascript:;">${chilItem.name }</a>
						</dd>
						</c:if>
						</c:forEach>
					</dl>
				</li>
				</c:forEach>
			</ul>
		</div>
	</div>
	<div class="layui-body layui-body-xsm">
		<div class="layui-tab layui-tab-card main-index-tab" lay-filter="F_index_tab">
			<ul class="layui-tab-title" id="J_index_tab_title">
				<li class="layui-this">首页</li>
			</ul>
			<div class="layui-tab-content">
				<div class="layui-tab-item layui-show">
					<iframe name="mainFrame" frameborder="0" src="${__URL__ }/welcome.jsp"></iframe>
				</div>
			</div>
		</div>
	</div>
	<div class="layui-footer layui-footer-xsm main-index-footer">
		<div class="layui-main">
			<p>© 2017 jsyso ${fns:getConfig('text.productName')} ${fns:getConfig('text.productVersion')} ${fns:getConfig('text.productMsg')} </p>
		</div>
	</div>
</div>
<script type="text/javascript">
layui.use(['element'], function(){
	var $ = layui.jquery
	,layer = layui.layer
	,element = layui.element();
	
	//iframe自适应
    $(window).on('resize', function () {
        var $content = $('.main-index-tab .layui-tab-content')
        ,headHeight = $(".frame-header").height()
        ,tabHeight = $(".layui-tab-title").height()
        ,footHeight = $(".main-index-footer").outerHeight();
        $content.height($(this).height() - headHeight - tabHeight - footHeight - 30);
    }).resize();
	
    function _openNav() {
    	var elem = $(this)
    	,url = elem.data('url')
    	,id = elem.data('id')
    	,$tabTitle = $("#J_index_tab_title");
    	// 是否已经存在iframe
    	if(!$tabTitle.children("li[lay-id='"+id+"']").length) {
    		var title = elem.text() + '<i class="layui-icon layui-unselect layui-tab-close" data-id="'+id+'">&#x1006;</i>'
    		,content = '<iframe id="J_iframe_'+id+'" data-id="'+id+'" frameborder="0" src="${__ADMIN__ }'+url+'"></iframe>';
    		element.tabAdd('F_index_tab', {
    			  title: title
    			  ,content: content
    			  ,id: id
    			});
    		//监听关闭事件
    		$tabTitle.find('i.layui-tab-close').on('click', function() {
    			element.tabDelete('F_index_tab', $(this).data('id'));
    		});
    	}else {
    		$('#J_iframe_' + id).attr('src', "${__ADMIN__ }"+url);  
    	}
    	// 切换到当前选项卡
    	element.tabChange('F_index_tab', id);
    	// 关闭菜单
    	$(".layui-side-xsm").hide();
    }
	// 菜单点击打开iframe
    element.on('nav(F_index_nav)', _openNav);
	$(".do-open-nav").on('click', _openNav);
	$("#J_open_nav").on('click', function() {
		$(".layui-side-xsm").toggle();
	});
});
</script>
</body>
</html>