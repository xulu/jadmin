<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/layui/_public/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>首页</title>
<%@include file="/WEB-INF/views/layui/_public/header.jsp" %>
<style type="text/css">
* {
	box-sizing: border-box;
}
.site-content {
	width: 100%;
    display: inline-block;
    vertical-align: top;
    font-size: 14px;
    padding: 10px;
}
.site-h1 {
    margin-bottom: 20px;
    line-height: 60px;
    padding-bottom: 10px;
    color: #393D49;
    border-bottom: 1px solid #eee;
    font-size: 28px;
    font-weight: 300;
}
.site-tips {
    margin-bottom: 10px;
    padding: 15px;
    line-height: 22px;
    border-left: 5px solid #0078AD;
    background-color: #f2f2f2;
}
.site-text {
    position: relative;
}
.site-title {
    margin: 20px 0 20px;
}
</style>
</head>
<body>
<div class="site-content">
	<blockquote class="site-tips site-text">
	欢迎进入jadmin后台管理系统，jadmin是基于jsyso + LayUI搭建的Java后台管理系统，目前已自带权限控制模块，多选项卡操作。
    </blockquote>
    <fieldset class="layui-elem-field layui-field-title site-title">
		<legend>jsyso</legend>
    </fieldset>
    <blockquote class="layui-elem-quote jadmin-quote-label"><i class="layui-icon">&#xe64c;</i>jsyso：基于Spring JDBC的最简开发框架。<a href="http://git.oschina.net/xujian_jason/jsyso" target="_blank">http://git.oschina.net/xujian_jason/jsyso</a></blockquote>
    <fieldset class="layui-elem-field layui-field-title site-title">
		<legend>jadmin</legend>
    </fieldset>
    <blockquote class="layui-elem-quote jadmin-quote-label"><i class="layui-icon">&#xe64c;</i>jadmin是基于jsyso + LayUI搭建的后台管理系统，目前已自带权限控制模块，多选项卡操作。<a href="http://git.oschina.net/xujian_jason/jadmin" target="_blank">http://git.oschina.net/xujian_jason/jadmin</a></blockquote>
    <fieldset class="layui-elem-field layui-field-title site-title">
		<legend>jsyso-weixin</legend>
    </fieldset>
    <blockquote class="layui-elem-quote jadmin-quote-label"><i class="layui-icon">&#xe64c;</i>jsyso-weixin是基于jsyso的极速微信开发框架，以处理器方式开发微信，无任何继承关系，对代码无侵入。<a href="http://git.oschina.net/xujian_jason/jsyso-weixin" target="_blank">http://git.oschina.net/xujian_jason/jsyso-weixin</a></blockquote>
</div>
<script type="text/javascript">
layui.use(['element'], function(){
	var $ = layui.jquery
	,layer = layui.layer
	,element = layui.element();
	
});
</script>
</body>
</html>